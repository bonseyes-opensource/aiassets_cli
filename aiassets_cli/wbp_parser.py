import argparse


def add_wbp_subparser(subparser):
    # DEMO
    demo_parser = subparser.add_parser('demo', help="AI Asset demo.")
    demo_subparser = demo_parser.add_subparsers(help='demo_commands', dest='demo_commands')
    demo_arguments = argparse.ArgumentParser(add_help=False)
    demo_arguments.add_argument(
        '--input-size', '-is',
        required=False, type=str,
        default='120x120',
        help='Model input size in format: WIDTHxHEIGHT')
    demo_arguments.add_argument(
        '--engine', '-en', nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'lpdnn'],
        help='Inference engine: pytorch | onnxruntime | tensorrt | lpdnn',
    )
    demo_arguments.add_argument(
        '--precision', '-pr', required=False, default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Precision options: fp32 | fp16 | int8',
    )
    demo_arguments.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='shufflenetv2k30',
        default='shufflenetv2k30',
        choices=['shufflenetv2k30', 'shufflenetv2k16', 'resnet50', 'mobilenetv2', 'mobilenetv3small',
                 'mobilenetv3large', 'swin-b', 'swin-s', 'swin-t'],
        help='Available backbones: shufflenetv2k30 | shufflenetv2k16 | resnet50 | mobilenetv2 | mobilenetv3small | mobilenetv3large | swin-b | swin-s | swin-t',
    )
    demo_arguments.add_argument(
        '--device', '-de', required=False, default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    demo_arguments.add_argument(
        '--cpu-num', '-cn', required=False, default=None, type=int,
        help='Number of cpu cores: e.g 12',
    )
    demo_arguments.add_argument(
        '--thread-num', '-tn', required=False, default=None, type=int,
        help='Number of threads: e.g 12',
    )
    demo_arguments.add_argument(
        '--render',
        nargs='?',
        const='2d_sparse',
        default='2d_sparse',
        choices=['2d_sparse', '2d_dense', '3d', 'pose', 'axis'],
        help='Rendering landmarks: 2d_sparse, 2d_dense, 3d, pose',
    )
    demo_arguments.add_argument(
        "--thickness",
        '-t',
        required=False,
        type=int,
        default=1,
        choices=range(1, 21),
        metavar="[1-20]",
        help="Thickness of the visualization overlay in pixels.",
    )

    demo_arguments.add_argument("--width", '-wi', required=False, help="Width in pixels.")
    demo_arguments.add_argument("--height", '-he', required=False, help="Height in pixels.")
    demo_arguments.add_argument("--color", '-co', required=False, help="Color format: GRAY | BGR.")
    demo_arguments.add_argument('--rotate', '-ro', required=False, type=int, choices=[90, -90, 180],
                                help='Rotation options: 90 | -90 | 180',
                                )

    # IMAGE
    image_args = demo_subparser.add_parser('image', parents=[demo_arguments], help="AI Asset demo image.")
    image_args.add_argument("--image-input", '-ii', required=True, help="Image path to process.")

    # VIDEO
    video_args = demo_subparser.add_parser('video', parents=[demo_arguments], help="AI Asset demo video.")
    video_args.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    video_args.add_argument("--video-input", '-vi', required=True, help="Video path to process.")

    # CAMERA
    camera_args = demo_subparser.add_parser('camera', parents=[demo_arguments], help="AI Asset demo camera.")
    camera_args.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    camera_args.add_argument(
        '--camera-id',
        '-ci',
        required=False,
        default=0,
        help="Camera ID",
    )

    # SERVER
    server_parser = subparser.add_parser('server', help="AI Asset server.")
    server_subparser = server_parser.add_subparsers(help='server_commands', dest='server_commands')
    server_arguments = argparse.ArgumentParser(add_help=False)

    # SERVER START
    server_start_arguments = server_subparser.add_parser('start', parents=[server_arguments],
                                                         help="AI Asset server start.")

    server_start_arguments.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='120x120',
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )

    server_start_arguments.add_argument(
        '--engine', '-en', required=False,
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    server_start_arguments.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='shufflenetv2k30',
        default='shufflenetv2k30',
        choices=['shufflenetv2k30', 'shufflenetv2k16', 'resnet50', 'mobilenetv2', 'mobilenetv3small', 'mobilenetv3large', 'swin-b', 'swin-s', 'swin-t' ],
        help='Available backbones: shufflenetv2k30 | shufflenetv2k16 | resnet50 | mobilenetv2 | mobilenetv3small | mobilenetv3large | swin-b | swin-s | swin-t',
    )
    server_start_arguments.add_argument(
        '--precision', '-pr',
        required=False,
        nargs='?',
        const='fp32',
        default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Model precision: fp32, fp16. Default: fp32'
    )
    server_start_arguments.add_argument(
        '--device', '-de', required=False, default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    server_start_arguments.add_argument(
        '--cpu-num', '-cn', required=False, default=None, type=int,
        help='Number of cpu cores: e.g 12',
    )
    server_start_arguments.add_argument(
        '--thread-num', '-tn', required=False, default=None, type=int,
        help='Number of threads: e.g 12',
    )

    # SERVER STOP
    server_stop_arguments = server_subparser.add_parser('stop', parents=[server_arguments],
                                                        help="AI Asset server stop.")

    # EXPORT
    export_args = subparser.add_parser('export', help="AI Asset model export.")
    export_args.add_argument(
        "--export-input-sizes",
        '-eis',
        nargs='+',
        required=True,
        default=['120x120'],
        type=str,
        help='Specify target input sizes: w1xh1 w2xh2 ...'
    )
    export_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'lpdnn', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | lpdnn | all',
    )
    export_args.add_argument(
        "--precisions", '-pcs',
        required=True,
        nargs='+',
        type=str,
        default='fp32',
        choices=['fp16', 'fp32'],
        help='Model precision: fp32, fp16. Default: fp32'
    )

    export_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='shufflenetv2k30',
        default='shufflenetv2k30',
        choices=['shufflenetv2k30', 'shufflenetv2k16', 'resnet50', 'mobilenetv2', 'mobilenetv3small',
                 'mobilenetv3large', 'swin-b', 'swin-s', 'swin-t'],
        help='Available backbones: shufflenetv2k30 | shufflenetv2k16 | resnet50 | mobilenetv2 | mobilenetv3small | mobilenetv3large | swin-b | swin-s | swin-t',
    )

    export_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    export_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    export_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )


    # OPTIMIZE
    optimize_args = subparser.add_parser('optimize', help="AI Asset model optimization.")
    optimize_args.add_argument(
        "--optimize-input-sizes", '-ois', nargs='+',
        required=True,
        default=['120x120'],
        type=str,
        help='Specify target input sizes: w1xh1 w2xh2 ...'
    )

    optimize_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )

    optimize_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='shufflenetv2k30',
        default='shufflenetv2k30',
        choices=['shufflenetv2k30', 'shufflenetv2k16', 'resnet50', 'mobilenetv2', 'mobilenetv3small',
                 'mobilenetv3large', 'swin-b', 'swin-s', 'swin-t'],
        help='Available backbones: shufflenetv2k30 | shufflenetv2k16 | resnet50 | mobilenetv2 | mobilenetv3small | mobilenetv3large | swin-b | swin-s | swin-t',
    )

    optimize_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    optimize_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    optimize_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # BENCHMARK
    benchmark_args = subparser.add_parser('benchmark', help="AI Asset benchmark.")
    benchmark_args.add_argument(
        "--benchmark-input-sizes",
        '-bis',
        required=False,
        default='120x120',
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    benchmark_args.add_argument(
        '--engine',
        '-e',
        required=False,
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'lpdnn'],
        help='Inference engine: pytorch | onnxruntime | tensorrt | lpdnn',
    )
    benchmark_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='shufflenetv2k30',
        default='shufflenetv2k30',
        choices=['shufflenetv2k30', 'shufflenetv2k16', 'resnet50', 'mobilenetv2', 'mobilenetv3small',
                 'mobilenetv3large', 'swin-b', 'swin-s', 'swin-t'],
        help='Available backbones: shufflenetv2k30 | shufflenetv2k16 | resnet50 | mobilenetv2 | mobilenetv3small | mobilenetv3large | swin-b | swin-s | swin-t',
    )
    benchmark_args.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    benchmark_args.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=False,
        const='wholebody', default='wholebody',
        choices=['wholebody', 'cocokp'],
        help='Available datasets: cocokp | wholebody',
    )
    benchmark_args.add_argument(
        '--precisions',
        '-pr',
        required=False,
        nargs='?',
        const='fp32',
        default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Model precision: fp32, fp16. Default: fp32'
    )
    benchmark_args.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs to use.',
    )
    benchmark_args.add_argument(
        '--threads-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads to use.',
    )

    # TRAIN
    train_parser = subparser.add_parser('train', help="AI Asset model train.")
    train_subparser = train_parser.add_subparsers(help='train_commands', dest='train_commands')
    train_args = argparse.ArgumentParser(add_help=False)

    # TRAIN START +++
    train_start_args = train_subparser.add_parser('start', parents=[train_args], help="AI Asset train start.")
    train_start_args.add_argument(
        '--config',
        '-cfg',
        nargs='?',
        required=True,
        choices=['v3.0_shufflenetv2k30_default_641x641_fp32_config',
                 'v3.0_shufflenetv2k16_default_641x641_fp32_config'],
        help='Available configs: v3.0_shufflenetv2k30_default_641x641_fp32_config | v3.0_shufflenetv2k16_default_641x641_fp32_config',
    )
    # TRAIN STOP
    train_stop_args = train_subparser.add_parser('stop', parents=[train_args], help="AI Asset train stop.")
