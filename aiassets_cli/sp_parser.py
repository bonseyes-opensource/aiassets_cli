import argparse


def add_sp_subparser(subparser):
    # DEMO
    demo_parser = subparser.add_parser('demo', help="AI Asset demo.")
    demo_subparser = demo_parser.add_subparsers(help='demo_commands', dest='demo_commands')
    demo_arguments = argparse.ArgumentParser(add_help=False)
    demo_arguments.add_argument(
        '--engine', '-en', required=True, default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'lpdnn'],
        help='Inference engine options: pytorch | onnxruntime | tensorrt | lpdnn',
    )
    demo_arguments.add_argument(
        '--precision', '-pr', required=False, default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Precision options: fp32 | fp16 | int8',
    )
    demo_arguments.add_argument(
        '--device', '-de', required=False, default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    demo_arguments.add_argument(
        '--cpu-num', '-cn', required=False, default=None, type=int,
        help='Number of cpu cores: e.g 12',
    )
    demo_arguments.add_argument(
        '--thread-num', '-tn', required=False, default=None, type=int,
        help='Number of threads: e.g 12',
    )

    # SIGNAL
    signal_args = demo_subparser.add_parser('signal', parents=[demo_arguments], help="AI Asset demo signal.")
    signal_args.add_argument(
        '--csv-input',
        '-ci',
        required=True,
        type=str,
        help='Path to csv input file',
    )

    # SERVER
    server_parser = subparser.add_parser('server', help="AI Asset server.")
    server_subparser = server_parser.add_subparsers(help='server_commands', dest='server_commands')
    server_arguments = argparse.ArgumentParser(add_help=False)

    # SERVER START
    server_start_arguments = server_subparser.add_parser('start', parents=[server_arguments],
                                                         help="AI Asset server start.")

    server_start_arguments.add_argument(
        '--input-size', '-is', required=False, type=str,
        help='Model input size in format NxCxHxW AxBxC XxYxWxZ')

    server_start_arguments.add_argument(
        '--engine', '-en', required=False, default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'lpdnn'],
        help='Inference engine options: pytorch | onnxruntime | tensorrt | lpdnn',
    )
    server_start_arguments.add_argument(
        '--backbone', '-bb', required=False,
        nargs='?',
        help='Backbone.',
    )
    server_start_arguments.add_argument(
        '--precision', '-pr', nargs='?',
        const='fp32',
        default='fp32',
        choices=['fp32', 'fp16', 'int8'],
        help='Model precision: fp32, fp16, int8. Default: fp32'
    )
    server_start_arguments.add_argument(
        '--device', '-de', required=False, default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    server_start_arguments.add_argument(
        '--cpu-num', '-cn', required=False, default=None, type=int,
        help='Number of cpu cores: e.g 12',
    )
    server_start_arguments.add_argument(
        '--thread-num', '-tn', required=False, default=None, type=int,
        help='Number of threads: e.g 12',
    )
    server_start_arguments.add_argument(
        '--lpdnn-engine',
        '-le',
        required=False,
        nargs='+',
        default=['onnxruntime'],
        choices=['onnxruntime', 'lne', 'tensorrt', 'ncnn'],
        help='lpdnn inference engine to use.',
    )

    # SERVER STOP
    server_stop_arguments = server_subparser.add_parser('stop', parents=[server_arguments],
                                                        help="AI Asset server stop.")

    # EXPORT +++
    export_args = subparser.add_parser('export', help="AI Asset model export.")
    export_args.add_argument(
        "--export-input-sizes",
        '-eis',
        nargs='+',
        type=str,
        required=False,
        help='Model input size in format: Dim1xDim2x...xDimN.',
    )
    export_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'lpdnn', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | lpdnn | all',
    )
    export_args.add_argument(
        "--precisions", '-pcs',
        required=False,
        nargs='+',
        type=str,
        default=['fp32', 'fp16'],
        choices=['fp32', 'fp16'],
        help='Model precision (fp32|fp16).',
    )

    export_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    export_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    export_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    export_args.add_argument(
        '--lpdnn-engine',
        '-le',
        required=False,
        nargs='+',
        default=['onnxruntime'],
        choices=['onnxruntime', 'lne', 'tensorrt', 'ncnn'],
        help='lpdnn inference engine to use.',
    )

    # OPTIMIZE
    optimize_args = subparser.add_parser('optimize', help="AI Asset model optimization.")
    optimize_args.add_argument(
        "--optimize-input-sizes", '-ois', nargs='+',
        required=False,
        default=[],
        type=str,
        help='One or more model input sizes in format NxCxHxW AxBxC XxYxWxZ',
    )

    optimize_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'lpdnn', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | lpdnn | all',
    )

    optimize_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    optimize_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    optimize_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # BENCHMARK +++
    benchmark_args = subparser.add_parser('benchmark', help="AI Asset benchmark.")
    benchmark_args.add_argument(
        "--benchmark-input-sizes",
        '-bis',
        nargs='+',
        required=False,
        type=str,
        default=[],
        help='one or more model input sizes in format NxCxHxW AxBxC XxYxWxZ',
    )
    benchmark_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'lpdnn', 'all'],
        help='Choose one or more engines: pytorch | onnxruntime | tensorrt | lpdnn | all',
    )
    benchmark_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='-',
        default='-',
        required=False,
        help='Backbone.',
    )
    benchmark_args.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    benchmark_args.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=False,
        default='all',
        type=str,
        help='One or more datasets to use: all | <dataset_name>',
    )
    benchmark_args.add_argument(
        '--precisions',
        '-pr',
        required=False,
        nargs='+',
        type=str,
        default=['all'],
        choices=['fp16', 'fp32', 'int8', 'all'],
        help='One or more precisions to include: fp16 | fp32 | int8 | all',
    )
    benchmark_args.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs to use.',
    )
    benchmark_args.add_argument(
        '--threads-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads to use.',
    )
    benchmark_args.add_argument(
        '--lpdnn-engine',
        '-le',
        required=False,
        nargs='+',
        default=['onnxruntime'],
        choices=['onnxruntime', 'lne', 'tensorrt', 'ncnn'],
        help='lpdnn inference engine to use.',
    )

    # TRAIN +++
    train_parser = subparser.add_parser('train', help="AI Asset model train.")
    train_subparser = train_parser.add_subparsers(help='train_commands', dest='train_commands')
    train_args = argparse.ArgumentParser(add_help=False)

    # TRAIN START
    train_start_args = train_subparser.add_parser('start', parents=[train_args], help="AI Asset train start.")
    train_start_args.add_argument(
        '--config',
        '-cfg',
        required=True,
        type=str,
        choices=[
            'v1.0_MyModel_default_1x3x16_fp32',
            'v1.0_MyModel_qat_1x3x16_fp32',
        ],
        help='Config file name.',
    )
    # TRAIN STOP
    train_stop_args = train_subparser.add_parser('stop', parents=[train_args], help="AI Asset train stop.")
