import argparse


def add_6dof_subparser(subparser):
    # DEMO
    demo_parser = subparser.add_parser('demo', help="AI Asset demo.")
    demo_subparser = demo_parser.add_subparsers(help='demo_commands', dest='demo_commands')
    demo_arguments = argparse.ArgumentParser(add_help=False)
    demo_arguments.add_argument('--engine', '-en', required=True, default='onnxruntime',
                                choices=['pytorch', 'onnxruntime'],
                                help='Inference engine options: pytorch | onnxruntime',
                                )
    demo_arguments.add_argument('--backbone', '-bb', required=False,
                                default='yolopose',
                                choices=['yolopose'],
                                help='Backbone options: yolopose',
                                )
    demo_arguments.add_argument('--precision', '-pr', required=False, default='fp32',
                                choices=['fp32'],
                                help='Precision options: fp32',
                                )
    demo_arguments.add_argument('--device', '-de', required=False, default='cpu',
                                choices=['gpu', 'cpu'],
                                help='Device options: gpu | cpu ',
                                )
    demo_arguments.add_argument('--cpu-num', '-cn', required=False, default=None, type=int,
                                help='Number of cpu cores: e.g 12',
                                )
    demo_arguments.add_argument('--thread-num', '-tn', required=False, default=None, type=int,
                                help='Number of threads: e.g 12',
                                )

    # IMAGE
    image_args = demo_subparser.add_parser('image', parents=[demo_arguments], help="AI Asset demo image.")
    image_args.add_argument("--image-input", '-ii', required=True, help="Image path to process.")

    # # VIDEO
    # video_args = demo_subparser.add_parser('video', parents=[demo_arguments], help="AI Asset demo video.")
    # video_args.add_argument(
    #     '--single-face-track',
    #     '-sft',
    #     required=False,
    #     default=False,
    #     dest='single_face_track',
    #     action='store_true',
    #     help="Track single face.",
    # )
    # video_args.add_argument("--video-input", '-vi', required=True, help="Video path to process.")
    #
    # # CAMERA
    # camera_args = demo_subparser.add_parser('camera', parents=[demo_arguments], help="AI Asset demo camera.")
    # camera_args.add_argument(
    #     '--single-face-track',
    #     '-sft',
    #     required=False,
    #     default=False,
    #     dest='single_face_track',
    #     action='store_true',
    #     help="Track single face.",
    # )
    # camera_args.add_argument(
    #     '--camera-id',
    #     '-ci',
    #     required=False,
    #     default=0,
    #     help="Camera ID",
    # )

    # SERVER
    server_parser = subparser.add_parser('server', help="AI Asset server.")
    server_subparser = server_parser.add_subparsers(help='server_commands', dest='server_commands')
    server_arguments = argparse.ArgumentParser(add_help=False)

    # SERVER START
    server_start_arguments = server_subparser.add_parser('start', parents=[server_arguments],
                                                         help="AI Asset server start.")
    server_start_arguments.add_argument('--engine', '-en', required=True, default='onnxruntime',
                                        choices=['pytorch', 'onnxruntime'],
                                        help='Inference engine options: tensorflow | onnxruntime',
                                        )
    server_start_arguments.add_argument('--backbone', '-bb', required=False,
                                        default='yolopose',
                                        choices=['yolopose'],
                                        help='Backbone options: yolopose',
                                        )
    server_start_arguments.add_argument('--device', '-de', required=False, default='cpu',
                                        choices=['gpu', 'cpu'],
                                        help='Device options: gpu | cpu ',
                                        )
    server_start_arguments.add_argument('--cpu-num', '-cn', required=False, default=None, type=int,
                                        help='Number of cpu cores: e.g 12',
                                        )
    server_start_arguments.add_argument('--thread-num', '-tn', required=False, default=None, type=int,
                                        help='Number of threads: e.g 12',
                                        )

    # SERVER STOP
    server_stop_arguments = server_subparser.add_parser('stop', parents=[server_arguments],
                                                        help="AI Asset server stop.")

    # EXPORT
    export_args = subparser.add_parser('export', help="AI Asset model export.")
    export_args.add_argument("--export-input-sizes", '-eis', nargs='+', type=str, required=True, default=['120x120'],
                             help='Specify target input sizes: w1xh1 w2xh2 ...'
                             )
    export_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    export_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        default='yolopose',
        choices=['yolopose'],
        help='Available backbones: yolopose',
    )
    export_args.add_argument(
        "--precisions", '-pcs', nargs='+',
         required=True,
         default=['fp32'],
         choices=['fp16', 'fp32'],
         type=str,
         help='Specify target precisions: fp16 fp32'
    )
    export_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    export_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    export_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # OPTIMIZE
    optimize_args = subparser.add_parser('optimize', help="AI Asset model optimization.")
    optimize_args.add_argument(
        "--optimize-input-sizes",
        '-ois',
        nargs='+',
        type=str,
        required=True,
        default=['120x120'], help='Specify target input sizes: w1xh1 w2xh2 ...'
    )
    optimize_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    optimize_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        default='yolopose',
        choices=['yolopose'],
        help='Available backbones: yolopose',
    )
    optimize_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    optimize_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    optimize_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # BENCHMARK
    benchmark_args = subparser.add_parser('benchmark', help="AI Asset benchmark.")
    benchmark_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['pytorch', 'onnxruntime', 'all'],
        help='Choose one or more engines: pytorch | onnxruntime | all',
    )
    benchmark_args.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        default='yolopose',
        choices=['yolopose'],
        help='Available backbones: yolopose',
    )
    benchmark_args.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    benchmark_args.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=False,
        default='ITODD',
        type=str,
        choices=["ITODD"],
        help='Available devices: ITODD',
    )

    # TRAIN
    train_parser = subparser.add_parser('train', help="AI Asset model train.")
    train_subparser = train_parser.add_subparsers(help='train_commands', dest='train_commands')
    train_args = argparse.ArgumentParser(add_help=False)

    # TRAIN START
    train_start_args = train_subparser.add_parser('start', parents=[train_args], help="AI Asset train start.")
    train_start_args.add_argument(
        '--config',
        '-cfg',
        required=True,
        type=str,
        choices=[
            'yolopose-mobilenet',
            'yolopose-resnet18',
            'yolopose-resnet50',
            'yolopose-tiny',
            'yolopose',
        ],
        help='Config file name.',
    )
    # TRAIN STOP
    train_stop_args = train_subparser.add_parser('stop', parents=[train_args], help="AI Asset train stop.")
