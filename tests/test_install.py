import os
import pytest
import subprocess


def test_aiassets_cli_install():
    rc = subprocess.call(
        """pip3 install . && \
           export PATH=$PATH:/home/${USER}/.local/bin""",
        shell=True)
    assert rc == 0


def test_entrypoint():
    rc = subprocess.call("bonseyes_aiassets_cli --help", shell=True)
    assert rc == 0
