import os
import pytest
import subprocess


def test_init_3dface_landmarks_bad_credentials():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task 3dface_landmarks \
            --platform x86_64 \
            --environment cpu \
            --version v1.0 \
            --user gitlab+wrong-token-000000 \
            --password bad_password""",
        shell=True)
    assert rc == 1

    rc = subprocess.call("docker stop 3dface_landmarks", shell=True)
    assert rc == 1


def test_init_3dface_landmarks_cpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task 3dface_landmarks \
            --platform x86_64 \
            --environment cpu \
            --version v1.0 \
            --user gitlab+deploy-token-483452 \
            --password zsEqp4321jiCzWS-TUaG""",
        shell=True)
    assert rc == 0


def test_demo_image_pytorch_3dface_landmarks_cpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine pytorch \
            --backbone mobilenetv1 \
            --image-input tests/samples/demo_image_3dface_landmarks.jpg \
            --input-size 120x120 \
            --device cpu""",
        shell=True)
    assert rc == 0

    rc = subprocess.call("docker stop 3dface_landmarks", shell=True)
    assert rc == 0


def test_init_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task 3dface_landmarks \
            --platform x86_64 \
            --environment cuda10.2_tensorrt7.0 \
            --version v1.0 \
            --user gitlab+deploy-token-483452 \
            --password zsEqp4321jiCzWS-TUaG""",
        shell=True)
    assert rc == 0


def test_export_all_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli export \
            --export-input-sizes 120x120 \
            --precisions fp16 fp32 \
            --engine all \
            --backbone mobilenetv1""",
        shell=True)
    assert rc == 0


def test_optimize_all_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli optimize \
            --optimize-input-sizes 120x120 \
            --engine all \
            --backbone mobilenetv1""",
        shell=True)
    assert rc == 0


def test_demo_image_pytorch_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine pytorch \
            --backbone mobilenetv1 \
            --image-input tests/samples/demo_image_3dface_landmarks.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_image_onnx_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine onnxruntime \
            --backbone mobilenetv1 \
            --image-input tests/samples/demo_image_3dface_landmarks.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_image_tensorrt_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine tensorrt \
            --backbone mobilenetv1 \
            --image-input tests/samples/demo_image_3dface_landmarks.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_video_tensorrt_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo video \
            --engine tensorrt \
            --backbone mobilenetv1 \
            --video-input tests/samples/demo_video_3dface_landmarks.mp4 \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_benchmark_all_3dface_landmarks_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli benchmark \
            --benchmark-input-sizes 120x120 \
            --dataset aflw2000-3d \
            --device gpu \
            --engine all \
            --backbone mobilenetv1""",
        shell=True)
    assert rc == 0


def test_shutdown_3dface_landmarks_gpu():
    rc = subprocess.call("docker stop 3dface_landmarks", shell=True)
    assert rc == 0
